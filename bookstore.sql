DROP SCHEMA IF EXISTS book_store;
CREATE SCHEMA book_store DEFAULT CHARACTER SET utf8;
USE book_store;

CREATE TABLE IF NOT EXISTS book (
	id BIGINT AUTO_INCREMENT,
	isbn VARCHAR(13) NOT NULL UNIQUE,
	name_of_book VARCHAR(75) NOT NULL,
	year_of_publication INT NOT NULL,
	short_description VARCHAR(255) NOT NULL,
	price DOUBLE NOT NULL,
	printing_house VARCHAR(255) NOT NULL,
	author VARCHAR(255) NOT NULL,
	number_pages INT NOT NULL,
		bookCover VARCHAR(75) NOT NULL,
	letter VARCHAR(75) NOT NULL,
	languages VARCHAR(50) NOT NULL,
	average_mark_book DOUBLE NOT NULL,
        quantity INT NOT NULL,
		picture longblob,

	PRIMARY KEY(id)
);    

CREATE TABLE IF NOT EXISTS genre (
	id BIGINT AUTO_INCREMENT,
	NAME VARCHAR(25) NOT NULL,
	DESCRIPTION VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS bookgenre (
    bookid BIGINT,
    genreid BIGINT,
    PRIMARY KEY(bookid, genreid),
    FOREIGN KEY(bookid) REFERENCES book(id)
		ON DELETE CASCADE,
    FOREIGN KEY(genreid) REFERENCES genre(id)
		ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS user (
	id BIGINT AUTO_INCREMENT,
	username VARCHAR(20) NOT NULL UNIQUE,
	user_password VARCHAR(20) NOT NULL,
    email VARCHAR(20) NOT NULL,
    firstname VARCHAR(20) NOT NULL,
    lastname VARCHAR(20) NOT NULL,
    address VARCHAR(20) NOT NULL,
    telephone_number VARCHAR(20) NOT NULL,
    user_role VARCHAR(20) NOT NULL,
    registration_time DATETIME(6) NOT NULL,

	PRIMARY KEY(id)
);    
INSERT INTO `book_store`.`user` (`username`, `user_password`, `email`, `firstname`, `lastname`, `address`, `telephone_number`, `user_role`, `registration_time`) VALUES ('admin', 'admin', 'admin@gmail.com', 'admin', 'admin', 'admin', '065141121', 'admin', '2021-01-09 21:25:04.075000');
INSERT INTO `book_store`.`user` (`username`, `user_password`, `email`, `firstname`, `lastname`, `address`, `telephone_number`, `user_role`, `registration_time`) VALUES ('buyer', 'buyer', 'buyer@gmail.com', 'buyer', 'buyer', 'buyer', '067213457', 'buyer', '2021-01-09 21:25:04.075000');
INSERT INTO `book_store`.`user` (`username`, `user_password`, `email`, `firstname`, `lastname`, `address`, `telephone_number`, `user_role`, `registration_time`) VALUES ('blocked', 'blocked', 'blocked@gmail.com', 'blocked', 'blocked', 'blocked', '06045741', 'blocked', '2021-01-09 21:25:04.075000');

INSERT INTO `book_store`.`book` (`isbn`, `name_of_book`, `short_description`, `year_of_publication`, `price`, `printing_house`, `author`, `number_pages`, `bookCover`, `letter`, `languages`, `average_mark_book`, `quantity`) VALUES ('1231231231231', 'FASHION MANIFESTO', 'Short description Faschion Manifesto', '1997', '590', 'Thames and Hudson', 'GABRIELLE CHANEL', '250', 'hard', 'latynic', 'english', '4.11', '250');
INSERT INTO `book_store`.`book` (`isbn`, `name_of_book`, `short_description`, `year_of_publication`, `price`, `printing_house`, `author`, `number_pages`, `bookCover`, `letter`, `languages`, `average_mark_book`, `quantity`) VALUES ('3213213213211', 'COLD STORAGE', 'Short description Cold Storage', '2014', '450', 'Harper Collins Publishers Ltd', 'DAVID KOEPP', '111', 'soft', 'latynic', 'english', '4.24', '114');
INSERT INTO `book_store`.`book` (`isbn`, `name_of_book`, `short_description`, `year_of_publication`, `price`, `printing_house`, `author`, `number_pages`, `bookCover`, `letter`, `languages`, `average_mark_book`, `quantity`) VALUES ('1311311311311', 'ORFEIA', 'Short description Orfeia', '2011', '750', 'Orion', 'JOANNE M HARRIS', '420', 'hard', 'cyrilic', 'serbian', '3.95', '90');


INSERT INTO `book_store`.`genre` (`NAME`, `DESCRIPTION`) VALUES ('epic', 'epic description');
INSERT INTO `book_store`.`genre` (`NAME`, `DESCRIPTION`) VALUES ('drama', 'drama description');
INSERT INTO `book_store`.`genre` (`NAME`, `DESCRIPTION`) VALUES ('adventure', 'adventure description');

INSERT INTO `book_store`.`bookgenre` (`bookid`, `genreid`) VALUES ('1', '1');
INSERT INTO `book_store`.`bookgenre` (`bookid`, `genreid`) VALUES ('1', '2');
INSERT INTO `book_store`.`bookgenre` (`bookid`, `genreid`) VALUES ('2', '3');
INSERT INTO `book_store`.`bookgenre` (`bookid`, `genreid`) VALUES ('3', '1');

DROP TABLE IF EXISTS shopping_cart;

CREATE TABLE IF NOT EXISTS shopping_cart (
  quantity int(11) DEFAULT NULL,
  price double DEFAULT NULL,
  book_id bigint(20) NOT NULL,
  user_id bigint(20) NOT NULL,
  PRIMARY KEY (`book_id`, `user_id`),
  KEY `FK_shopping_cart_book_id` (`book_id`),
  KEY `FK_shopping_cart_user_id` (`user_id`),
  CONSTRAINT `FK_shopping_cart_book_id` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`),
  CONSTRAINT `FK_shopping_cart_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);


CREATE TABLE IF NOT EXISTS `loyalty_card` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `points` int(11) DEFAULT NULL,
  `status_card` varchar(10) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_loyalty_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);


CREATE TABLE IF NOT EXISTS `comment` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `text_comment` VARCHAR(255) NOT NULL,
  `mark` INT NOT NULL,
  `book_id` BIGINT(20) NOT NULL,
  `user_id` BIGINT(20) NOT NULL,
  `date_of_comment` DATE NOT NULL,
  `status_comment` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_comment_book_id` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`),
  CONSTRAINT `FK_comment_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);

INSERT INTO `book_store`.`comment` (`text_comment`, `mark`, `book_id`, `user_id`, `date_of_comment`, `status_comment`) VALUES ('The book is great', '5', '1', '3', '2021-11-05', 'hold');
INSERT INTO `book_store`.`comment` (`text_comment`, `mark`, `book_id`, `user_id`, `date_of_comment`, `status_comment`) VALUES ('Excelent', '5', '1', '2', '2021-11-05', 'hold');
INSERT INTO `book_store`.`comment` (`text_comment`, `mark`, `book_id`, `user_id`, `date_of_comment`, `status_comment`) VALUES ('Very good', '4', '2', '3', '2021-10-05', 'approved');

CREATE TABLE IF NOT EXISTS `purchase` (
  `purchase_id` BIGINT(11) NOT NULL AUTO_INCREMENT,
  `total_price_of_purchase` double DEFAULT NULL,
  `time_of_purchase` datetime(6) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `number_of_purchased_books` INT,
  PRIMARY KEY (`purchase_id`),
  CONSTRAINT `FK_purchase_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);


CREATE TABLE IF NOT EXISTS `purchased_book` (
  `purchased_book_id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `quantity` INT(11) DEFAULT NULL,
  `price` DOUBLE DEFAULT NULL,
  `book_id` BIGINT(20) NOT NULL,
  `user_id` BIGINT(20) NOT NULL,
  `purchase_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`purchased_book_id`),
  CONSTRAINT `FK_purchased_book_book_id` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`),
  CONSTRAINT `FK_purchased_book_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_purchased_book_purchase_id` FOREIGN KEY (`purchase_id`) REFERENCES `purchase` (`purchase_id`)
);

CREATE TABLE IF NOT EXISTS `wishlist` (
  `book_id`  bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`book_id`, `user_id`),
  KEY `FK_wishlist_book_id` (`book_id`),
  KEY `FK_wishlist_user_id` (`user_id`),
  CONSTRAINT `FK_wishlist_book_id` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`),
  CONSTRAINT `FK_wishlist_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);
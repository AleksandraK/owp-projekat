package com.bookstore.app.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import com.bookstore.app.model.PurchasedBook;

public interface PurchasedBookService {
	
public Optional<PurchasedBook> findById(String id);
	
	public List<PurchasedBook> findAllByPurchaseId(Long purchaseId);
	
	public List<PurchasedBook> findAll();
	
	public List<PurchasedBook> findAllByTimeOfPurchase(LocalDateTime startDate, LocalDateTime endDate);
	
	public void save(PurchasedBook purchasedBook);
	
	public List<PurchasedBook> findAllByPurchaseTimeOfPurchase(LocalDateTime dateTime);

}

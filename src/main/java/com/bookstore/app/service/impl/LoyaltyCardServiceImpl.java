package com.bookstore.app.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookstore.app.dao.LoyaltyCardDAO;
import com.bookstore.app.model.LoyaltyCard;
import com.bookstore.app.service.LoyaltyCardService;

@Service
public class LoyaltyCardServiceImpl implements LoyaltyCardService{
	
	@Autowired
	private LoyaltyCardDAO loyaltyCardDAO;

	@Override
	public LoyaltyCard findById(Long id) {
		return loyaltyCardDAO.findById(id);
	}

	@Override
	public List<LoyaltyCard> findAll() {
		return loyaltyCardDAO.findAll();
	}

	@Override
	public LoyaltyCard findByUserId(Long userId) {
		return loyaltyCardDAO.findByUserId(userId);
	}

	@Override
	public void save(LoyaltyCard loyaltyCard) {
		loyaltyCardDAO.save(loyaltyCard);
		
	}

	@Override
	public void update(LoyaltyCard loyaltyCard) {
		loyaltyCardDAO.update(loyaltyCard);
		
	}

	@Override
	public void delete(LoyaltyCard loyaltyCard) {
		loyaltyCardDAO.delete(loyaltyCard);
		
	}

	@Override
	public List<LoyaltyCard> findAllByStatusCard(String statusCard) {
		return loyaltyCardDAO.findAllByStatusCard(statusCard);
	}

	@Override
	public void updatePoints(LoyaltyCard loyaltyCard) {
		loyaltyCardDAO.updatePoints(loyaltyCard);
		
	}

	@Override
	public double usePointsForDiscount(int points, double price) {
		return loyaltyCardDAO.usePointsForDiscount(points, price);
	}

}

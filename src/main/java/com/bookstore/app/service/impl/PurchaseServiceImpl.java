package com.bookstore.app.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookstore.app.dao.PurchaseDAO;
import com.bookstore.app.model.Purchase;
import com.bookstore.app.service.PurchaseService;

@Service
public class PurchaseServiceImpl implements PurchaseService{
	
	@Autowired
	private PurchaseDAO purchaseDAO;

	@Override
	public Optional<Purchase> findById(Long id) {
		return purchaseDAO.findById(id);
	}

	@Override
	public List<Purchase> findAllByUserId(Long userId) {
		return purchaseDAO.findAllByUserId(userId);
	}

	@Override
	public List<Purchase> findAll() {
		return purchaseDAO.findAll();
	}

	@Override
	public void save(Purchase purchase) {
		purchaseDAO.save(purchase);	
	}

}

package com.bookstore.app.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookstore.app.dao.CommentDAO;
import com.bookstore.app.model.Comment;
import com.bookstore.app.service.CommentService;

@Service
public class CommentServiceImpl implements CommentService {

	@Autowired
	private CommentDAO commentDAO;

	@Override
	public Comment findById(Long id) {
		return commentDAO.findById(id);
	}

	@Override
	public List<Comment> findAll() {
		// TODO Auto-generated method stub
		return commentDAO.findAll();
	}

	@Override
	public Comment findByUserId(Long userId) {
		return commentDAO.findByUserId(userId);
	}

	@Override
	public void save(Comment comment) {
		commentDAO.save(comment);
		
	}

	@Override
	public void update(Comment comment) {
		commentDAO.update(comment);
		
	}

	@Override
	public void delete(Comment comment) {
		commentDAO.delete(comment);
		
	}

	@Override
	public List<Comment> findAllByStatusComment(String statusComment) {
		return commentDAO.findAllByStatusComment(statusComment);
	}

}

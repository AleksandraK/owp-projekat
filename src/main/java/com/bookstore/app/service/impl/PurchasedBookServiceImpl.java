package com.bookstore.app.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookstore.app.dao.PurchasedBookDAO;
import com.bookstore.app.model.PurchasedBook;
import com.bookstore.app.service.PurchasedBookService;

@Service
public class PurchasedBookServiceImpl implements PurchasedBookService{
	
	@Autowired
	private PurchasedBookDAO purchasedBookDAO;

	@Override
	public Optional<PurchasedBook> findById(String id) {
		return purchasedBookDAO.findById(id);
	}

	@Override
	public List<PurchasedBook> findAllByPurchaseId(Long purchaseId) {
		return purchasedBookDAO.findAllByPurchaseId(purchaseId);
	}

	@Override
	public List<PurchasedBook> findAll() {
		return purchasedBookDAO.findAll();
	}

	@Override
	public List<PurchasedBook> findAllByTimeOfPurchase(LocalDateTime startDate, LocalDateTime endDate) {
		return purchasedBookDAO.findAllByTimeOfPurchase(startDate, endDate);
	}

	@Override
	public void save(PurchasedBook purchasedBook) {
		purchasedBookDAO.save(purchasedBook);
		
	}

	@Override
	public List<PurchasedBook> findAllByPurchaseTimeOfPurchase(LocalDateTime dateTime) {
		return purchasedBookDAO.findAllByPurchaseTimeOfPurchase(dateTime);
	}
	
	



}

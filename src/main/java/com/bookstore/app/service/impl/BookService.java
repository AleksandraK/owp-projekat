package com.bookstore.app.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.bookstore.app.model.Book;

@Service
public interface BookService {
	
	public List<Book> getAllBooks();

	public Book findBookById(Long id);
	
	public void createBook(Book book);
	
	public void updateBook(Book book);
	
}

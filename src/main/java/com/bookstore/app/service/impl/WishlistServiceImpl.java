package com.bookstore.app.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookstore.app.dao.WishlistDAO;
import com.bookstore.app.model.User;
import com.bookstore.app.model.Wishlist;
import com.bookstore.app.service.WishlistService;

@Service
public class WishlistServiceImpl implements WishlistService {

	@Autowired
	private WishlistDAO wishlistDAO;

	@Override
	public Optional<Wishlist> findById(Wishlist wishlist) {
		return wishlistDAO.findById(wishlist);
	}

	@Override
	public List<Wishlist> findAll() {
		return wishlistDAO.findAll();
	}

	@Override
	public List<Wishlist> findAllByUserId(Long userId) {
		return wishlistDAO.findAllByUserId(userId);
	}

	@Override
	public List<Wishlist> findAllByBookId(Long bookId) {
		return wishlistDAO.findAllByBookId(bookId);
	}

	@Override
	public void save(Wishlist wishlist) {
		wishlistDAO.save(wishlist);
		
	}

	@Override
	public void delete(Wishlist wishlist) {
		wishlistDAO.delete(wishlist);
		
	}

	@Override
	public void deleteByUserIdAndBookId(Wishlist wishlist) {
		wishlistDAO.deleteByUserIdAndBookId(wishlist);
		
	}

	@Override
	public List<Wishlist> findByUser(User user) {
		return wishlistDAO.findByUser(user);
	}

	@Override
	public Wishlist findByBookAndUserId(Long book_id, Long user_id) {
		return wishlistDAO.findByBookAndUserId(book_id, user_id);
	}	

}

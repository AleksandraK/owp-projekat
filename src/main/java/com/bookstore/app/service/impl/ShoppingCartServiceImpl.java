package com.bookstore.app.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookstore.app.dao.ShoppingCartDAO;
import com.bookstore.app.model.ShoppingCart;
import com.bookstore.app.service.ShoppingCartService;

@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {

	@Autowired
	private ShoppingCartDAO shoppingCartDAO;

	@Override
	public List<ShoppingCart> getAll() {
		return shoppingCartDAO.getAll();
	}

	@Override
	public ShoppingCart findById(ShoppingCart shoppingCart) {
		return shoppingCartDAO.findById(shoppingCart);
	}

	@Override
	public void updateShoppingCart(ShoppingCart shoppingCart) {
		shoppingCartDAO.updateShoppingCart(shoppingCart);
		
	}

	@Override
	public void createShoppingCart(ShoppingCart shoppingCart) {
		shoppingCartDAO.createShoppingCart(shoppingCart);
		
	}

	@Override
	public List<ShoppingCart> findByUserId(Long userId) {
		return shoppingCartDAO.findByUserId(userId);
	}

	@Override
	public ShoppingCart findByUserIdAndBookId(Long userId, Long bookId) {
		return shoppingCartDAO.findByUserIdAndBookId(userId, bookId);
	}

	@Override
	public void deleteByUserIdAndBookId(Long userId, Long bookId) {
		shoppingCartDAO.deleteByUserIdAndBookId(userId, bookId);	
	}
	

	
	

}

package com.bookstore.app.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.bookstore.app.model.ShoppingCart;

@Service
public interface ShoppingCartService {
	
	public List<ShoppingCart> getAll();

	public ShoppingCart findById(ShoppingCart shoppingCart);

	public void updateShoppingCart(ShoppingCart shoppingCart);

	public void createShoppingCart(ShoppingCart shoppingCart);

	public List<ShoppingCart> findByUserId(Long userId);
	
	public ShoppingCart findByUserIdAndBookId(Long userId, Long bookId);

	public void deleteByUserIdAndBookId(Long userId, Long bookId);
	
}

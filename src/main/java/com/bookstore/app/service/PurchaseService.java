package com.bookstore.app.service;

import java.util.List;
import java.util.Optional;

import com.bookstore.app.model.Purchase;

public interface PurchaseService {
	
	public Optional<Purchase> findById(Long id);
	
	public List<Purchase> findAllByUserId(Long userId);
	
	public List<Purchase> findAll();
	
	public void save(Purchase purchase);

}

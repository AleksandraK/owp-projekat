package com.bookstore.app.model;

public class ShoppingCart {
	
	private Integer quantity;
	
	private Double price;
	
	private Long bookId;
	
	private Long userId;

	public ShoppingCart(Integer quantity, Double price, Long bookId, Long userId) {
		this.quantity = quantity;
		this.price = price;
		this.bookId = bookId;
		this.userId = userId;
	}
	
	public ShoppingCart() {
		
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Long getBookId() {
		return bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}


package com.bookstore.app.model;

public class PurchasedBook {

	private Long purchasedBookId;
	
	private Integer quantity;
	
	private Double price;

	private Book book;
	
	private User user;
	
	private Purchase purchase;
	
	private double totalPrice;
	
	private int totalQuantity;

	
	
	public PurchasedBook(Long purchasedBookId, Integer quantity, Double price, Book book, User user,
			Purchase purchase) {
		super();
		this.purchasedBookId = purchasedBookId;
		this.quantity = quantity;
		this.price = price;
		this.book = book;
		this.user = user;
		this.purchase = purchase;
	}

	public PurchasedBook() {
		
	}


	public Long getPurchasedBookId() {
		return purchasedBookId;
	}



	public void setPurchasedBookId(Long purchasedBookId) {
		this.purchasedBookId = purchasedBookId;
	}



	public Integer getQuantity() {
		return quantity;
	}



	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}



	public Double getPrice() {
		return price;
	}



	public void setPrice(Double price) {
		this.price = price;
	}



	public Book getBook() {
		return book;
	}



	public void setBook(Book book) {
		this.book = book;
	}



	public User getUser() {
		return user;
	}



	public void setUser(User user) {
		this.user = user;
	}



	public Purchase getPurchase() {
		return purchase;
	}



	public void setPurchase(Purchase purchase) {
		this.purchase = purchase;
	}



	public double getTotalPrice() {
		return totalPrice;
	}



	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}



	public int getTotalQuantity() {
		return totalQuantity;
	}



	public void setTotalQuantity(int totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	
	
}


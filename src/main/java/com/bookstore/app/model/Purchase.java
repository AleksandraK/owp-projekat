package com.bookstore.app.model;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class Purchase implements Comparable<Purchase>{

	private Long purchaseId;
	
	private double totalPriceOfPurchase;
	
	private LocalDateTime timeOfPurchase;
	
	private User user;
	
	private int numberOfPurchasedBooks;
	
	private Set<PurchasedBook> purchasedBooks = new HashSet<>();

	
	public Purchase() {
		
	}

	public Purchase(Long purchaseId, double totalPriceOfPurchase, LocalDateTime timeOfPurchase, User user,
			int numberOfPurchasedBooks) {
		super();
		this.purchaseId = purchaseId;
		this.totalPriceOfPurchase = totalPriceOfPurchase;
		this.timeOfPurchase = timeOfPurchase;
		this.user = user;
		this.numberOfPurchasedBooks = numberOfPurchasedBooks;
	}

	public Long getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(Long purchaseId) {
		this.purchaseId = purchaseId;
	}

	public double getTotalPriceOfPurchase() {
		return totalPriceOfPurchase;
	}

	public void setTotalPriceOfPurchase(double totalPriceOfPurchase) {
		this.totalPriceOfPurchase = totalPriceOfPurchase;
	}

	public LocalDateTime getTimeOfPurchase() {
		return timeOfPurchase;
	}

	public void setTimeOfPurchase(LocalDateTime timeOfPurchase) {
		this.timeOfPurchase = timeOfPurchase;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getNumberOfPurchasedBooks() {
		return numberOfPurchasedBooks;
	}

	public void setNumberOfPurchasedBooks(int numberOfPurchasedBooks) {
		this.numberOfPurchasedBooks = numberOfPurchasedBooks;
	}
	
	public Set<PurchasedBook> getPurchasedBook() {
		return this.purchasedBooks;
	}
	
	public void addPurchasedBook(PurchasedBook purchasedBook) {
		this.purchasedBooks.add(purchasedBook);
	}

	@Override
	public int compareTo(Purchase secondPurchase) {
		return secondPurchase.timeOfPurchase.compareTo(this.timeOfPurchase);
	}	
}

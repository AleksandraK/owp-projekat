package com.bookstore.app.model;

public class Wishlist {

	private Book book;
	
	private User user;

	public Wishlist(Book book, User user) {
		this.book = book;
		this.user = user;
	}
	
	public Wishlist() {
		
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
}


package com.bookstore.app.model;

public class LoyaltyCard {
	
	public static final String HOLD = "hold";
	public static final String APPROVED = "approved";
	public static final String REJECTED = "rejected";
	
	private Long id;
	
	private Integer points;
	
	private Long userId;
	
	private String statusCard;

	public LoyaltyCard(Long id, Integer points, Long userId, String statusCard) {
		super();
		this.id = id;
		this.points = points;
		this.userId = userId;
		this.statusCard = statusCard;
	}
	
	public LoyaltyCard() {
	}
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getStatusCard() {
		return statusCard;
	}

	public void setStatusCard(String statusCard) {
		this.statusCard = statusCard;
	}
	
	
	
	

}

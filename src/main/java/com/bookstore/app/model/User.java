package com.bookstore.app.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class User {

	public static final String BUYER = "buyer";
	public static final String ADMIN = "admin";
	public static final String BLOCKED = "blocked";

	private Long id;

	private String username;

	private String password;

	private String email;

	private String firstname;

	private String lastname;

	private String address;

	private String telephoneNumber;

	private String role;

	private LocalDateTime registrationTime;

	private boolean hasLoyaltyCard;

	private boolean isLoyaltyCardOnHold;

	private List<Book> shoppingCartBooks = new ArrayList<>();

	private boolean isPurchasedBook;

	private Set<PurchasedBook> purchasedBook = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public List<Book> getShoppingCartBooks() {
		return shoppingCartBooks;
	}

	public void setShoppingCartBooks(List<Book> shoppingCartBooks) {
		this.shoppingCartBooks = shoppingCartBooks;
	}

	public LocalDateTime getRegistrationTime() {
		return registrationTime;
	}

	public void setRegistrationTime(LocalDateTime registrationTime) {
		this.registrationTime = registrationTime;
	}

	public boolean isHasLoyaltyCard() {
		return hasLoyaltyCard;
	}

	public void setHasLoyaltyCard(boolean hasLoyaltyCard) {
		this.hasLoyaltyCard = hasLoyaltyCard;
	}

	public boolean isLoyaltyCardOnHold() {
		return isLoyaltyCardOnHold;
	}

	public void setLoyaltyCardOnHold(boolean isLoyaltyCardOnHold) {
		this.isLoyaltyCardOnHold = isLoyaltyCardOnHold;
	}



	public boolean isPurchasedBook() {
		return isPurchasedBook;
	}

	public void setPurchasedBook(boolean isPurchasedBook) {
		this.isPurchasedBook = isPurchasedBook;
	}

	public Set<PurchasedBook> getPurchasedBook() {
		return purchasedBook;
	}

	public void setPurchasedBook(Set<PurchasedBook> purchasedBook) {
		this.purchasedBook = purchasedBook;
	}

	public List<String> getAllRoles() {
		List<String> roles = new ArrayList<>();
		roles.add(BLOCKED);
		roles.add(BUYER);
		roles.add(ADMIN);

		return roles;
	}

}

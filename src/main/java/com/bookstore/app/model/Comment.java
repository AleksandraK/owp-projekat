package com.bookstore.app.model;

import java.sql.Date;

public class Comment implements Comparable<Comment> {
	
	public static final String APPROVED = "approved";
	public static final String HOLD = "hold";
	public static final String REJECTED = "rejected";
	
	private Long id;

	private String textComment;

	private Integer mark;

	private Long userId;

	private Long bookId;
	
	private Date dateOfComment;

	private String statusComment;

	
	public Comment(Long id, String textComment, Integer mark, Long userId, Long bookId, Date dateOfComment,
			String statusComment) {
		super();
		this.id = id;
		this.textComment = textComment;
		this.mark = mark;
		this.userId = userId;
		this.bookId = bookId;
		this.dateOfComment = dateOfComment;
		this.statusComment = statusComment;
	}
	
	public Comment() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTextComment() {
		return textComment;
	}

	public void setTextComment(String textComment) {
		this.textComment = textComment;
	}

	public Integer getMark() {
		return mark;
	}

	public void setMark(Integer mark) {
		this.mark = mark;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getBookId() {
		return bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	public Date getDateOfComment() {
		return dateOfComment;
	}

	public void setDateOfComment(Date dateOfComment) {
		this.dateOfComment = dateOfComment;
	}

	public String getStatusComment() {
		return statusComment;
	}

	public void setStatusComment(String statusComment) {
		this.statusComment = statusComment;
	}
	
	@Override
	public int compareTo(Comment comment) {
		return dateOfComment.compareTo(comment.getDateOfComment());
	}
	
	

}

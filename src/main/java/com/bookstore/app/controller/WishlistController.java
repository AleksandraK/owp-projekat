package com.bookstore.app.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.bookstore.app.model.Book;
import com.bookstore.app.model.User;
import com.bookstore.app.model.Wishlist;
import com.bookstore.app.service.BookService;
import com.bookstore.app.service.WishlistService;

@Controller
public class WishlistController {

	@Autowired
	private WishlistService wishlistService;

	@Autowired
	private BookService bookService;

	@GetMapping("/wishlist")
	public String showListOfBookInWishlist(Model model, HttpSession session) {
		User user = (User) session.getAttribute("currentUser");
		List<Wishlist> wishlist = wishlistService.findAllByUserId(user.getId());
		model.addAttribute("wishlist", wishlist);

		return "wishlist";
	}

	@PostMapping("/wishlist")
	public String addToWishlist(Model model, Wishlist wishlist, Long book_id, HttpSession session) {
		Book book = bookService.findBookById(book_id);
		User user = (User) session.getAttribute("currentUser");
		wishlist.setBook(book);
		wishlist.setUser(user);
		
		if (wishlistService.findById(wishlist) != null) {
			return showListOfBookInWishlist(model, session);
		}

			wishlistService.save(wishlist);

		return showListOfBookInWishlist(model, session);

	}

	@GetMapping("/wishlist/delete")
	public void deleteFromWishlist(@RequestParam(value = "userId") Long user_id, @RequestParam(value = "bookId") Long book_id) {
		Wishlist wishlist = wishlistService.findByBookAndUserId(book_id, user_id);
		wishlistService.deleteByUserIdAndBookId(wishlist);
	
	}

}
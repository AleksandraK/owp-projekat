package com.bookstore.app.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.bookstore.app.model.Purchase;
import com.bookstore.app.model.PurchasedBook;
import com.bookstore.app.service.PurchaseService;
import com.bookstore.app.service.PurchasedBookService;

@Controller
public class PurchaseController {

	@Autowired
	private PurchaseService purchaseService;

	@Autowired
	private PurchasedBookService purchasedBookService;

	@GetMapping("/admin/purchases")
	public String getPurchases(Model model) {
		if (model.getAttribute("purchasedBooks") == null) {
			List<PurchasedBook> purchasedBooks = new ArrayList<>();
			model.addAttribute("purchasedBooks", purchasedBooks);
		}

		return "show_purchases";
	}

	@PostMapping("/admin/purchases")
	public String getPurchasesFilter(Model model, String startDate, String endDate) {

		startDate += " 00:00";
		endDate += " 23:59";
		List<PurchasedBook> purchasedBooksModel = new ArrayList<>();
		String start = null;
		String end = null;
		double totalPriceOfBooks = 0.0;
		int totalQuantityOfBooks = 0;

		if ((startDate != null && !startDate.isEmpty()) && (endDate != null && !endDate.isEmpty())) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
			LocalDateTime startDateTime = LocalDateTime.parse(startDate, formatter);
			LocalDateTime endDateTime = LocalDateTime.parse(endDate, formatter);

			purchasedBooksModel = purchasedBookService.findAllByTimeOfPurchase(startDateTime, endDateTime);

			start = startDate.substring(0, 10);
			end = endDate.substring(0, 10);
		} else {
			purchasedBooksModel = purchasedBookService.findAll();

		}

		for (PurchasedBook purchasedBook : purchasedBooksModel) {
			totalQuantityOfBooks += purchasedBook.getTotalQuantity();
			double priceTwoDecimal = Math.round(purchasedBook.getTotalPrice() * 100.0d) / 100.0d;
			purchasedBook.setTotalPrice(priceTwoDecimal);
			totalPriceOfBooks += purchasedBook.getTotalPrice();
		}

		String totalPriceOfBooksString = String.format("%.2f", totalPriceOfBooks);

		model.addAttribute("purchasedBooks", purchasedBooksModel);
		model.addAttribute("totalQuantityOfBooks", totalQuantityOfBooks);
		model.addAttribute("totalPriceOfBooks", totalPriceOfBooksString);
		model.addAttribute("start", start);
		model.addAttribute("end", end);

		return getPurchases(model);
	}

	@GetMapping("admin/showPurchase/{userId}")
	public String showPurchasesOfUsersAdmin(Model model, @PathVariable Long userId) {

		List<Purchase> purchases = purchaseService.findAllByUserId(userId);
		Collections.sort(purchases);
		model.addAttribute("purchases", purchases);
		model.addAttribute("userId", userId);
		return "show_purchases_of_user";

	}

	@GetMapping("admin/purchase/{purchaseId}")
	public String showBooksOfUserPurchaseAdmin(Model model, @PathVariable Long purchaseId) {
		double totalPriceOfBook = 0.0;
		int totalQuantityOfBooks = 0;

		Purchase purchase = purchaseService.findById(purchaseId).get();
		List<PurchasedBook> purchasedBooks = purchasedBookService
				.findAllByPurchaseTimeOfPurchase(purchase.getTimeOfPurchase());

		for (PurchasedBook purchasedBook : purchasedBooks) {
			totalQuantityOfBooks += purchasedBook.getTotalQuantity();
			double priceTwoDecimal = Math.round(purchasedBook.getTotalPrice() * 100.0d) / 100.0d;
			purchasedBook.setTotalPrice(priceTwoDecimal);
			totalPriceOfBook += purchasedBook.getTotalPrice();
		}

		String totalPriceOfBookString = String.format("%.2f", totalPriceOfBook);

		model.addAttribute("purchase", purchase);
		model.addAttribute("purchasedBooks", purchasedBooks);
		model.addAttribute("totalQuantityOfBooks", totalQuantityOfBooks);
		model.addAttribute("totalPriceOfBook", totalPriceOfBookString);
		model.addAttribute("userId", purchase.getUser().getId());

		return "show_books_purchase";

	}
	
	@GetMapping("users/showPurchases/{userId}")
	public String showPurchases(Model model, @PathVariable Long userId) {

		List<Purchase> purchases = purchaseService.findAllByUserId(userId);
		Collections.sort(purchases);
		model.addAttribute("purchases", purchases);
		return "show_purchases_of_user";

	}

}

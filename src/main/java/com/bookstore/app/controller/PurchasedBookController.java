package com.bookstore.app.controller;

import java.time.LocalDateTime;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;

import com.bookstore.app.model.Book;
import com.bookstore.app.model.LoyaltyCard;
import com.bookstore.app.model.Purchase;
import com.bookstore.app.model.PurchasedBook;
import com.bookstore.app.model.ShoppingCart;
import com.bookstore.app.model.User;
import com.bookstore.app.service.BookService;
import com.bookstore.app.service.LoyaltyCardService;
import com.bookstore.app.service.PurchaseService;
import com.bookstore.app.service.PurchasedBookService;
import com.bookstore.app.service.ShoppingCartService;
import com.bookstore.app.service.UserService;

@Controller
public class PurchasedBookController {

	@Autowired
	private PurchasedBookService purchasedBookService;
	
	@Autowired
	private ShoppingCartService shoppingCardService;
	
	@Autowired
	private ShoppingCartController shoppingCartController;

	@Autowired
	private BookService bookService;

	@Autowired
	private UserService userService;
	
	@Autowired
	private PurchaseService purchaseService;
	
	@Autowired
	private LoyaltyCardService loyaltyCardService;
	
	@PostMapping("/purchasedBook")
	public String buyBooks(Model model, Long user_id, Integer points, HttpSession session) {
		Purchase purchase = new Purchase();
		int numberOfPurchasedBooks = 0;
		double totalPriceOfPurchase = 0.0;
		User user = userService.findUserById(user_id);
		List<ShoppingCart> shoppingCardList = shoppingCardService.findByUserId(user_id);
		for (ShoppingCart shoppingCard : shoppingCardList) {
			numberOfPurchasedBooks++;
			totalPriceOfPurchase+= shoppingCard.getPrice();
			}
			purchase.setTimeOfPurchase(LocalDateTime.now());
			purchase.setNumberOfPurchasedBooks(numberOfPurchasedBooks);
			purchase.setUser(user);

			double discount = 0;
			if (points!=null && points>0) {
				discount = totalPriceOfPurchase * (points*5) / 100;
			}
			
			purchase.setTotalPriceOfPurchase(totalPriceOfPurchase-discount);
			
			purchaseService.save(purchase);

			Purchase purchaseDb = purchaseService.findAll().get(purchaseService.findAll().size()-1);
		for (ShoppingCart shoppingCard : shoppingCardList) {
			PurchasedBook purchasedBook = new PurchasedBook();
			User shoppingCardUser = userService.findUserById(shoppingCard.getUserId());
			Book shoppingCardBook = bookService.findBookById(shoppingCard.getBookId());
			purchasedBook.setUser(shoppingCardUser);
			purchasedBook.setBook(shoppingCardBook);
			
			double discountByShoppingCard=0;
			if(points !=null && points>0) {
				discountByShoppingCard = shoppingCard.getPrice() * (points*5) / 100;
			}
			
			purchasedBook.setPrice(shoppingCard.getPrice()-discountByShoppingCard);
			purchasedBook.setQuantity(shoppingCard.getQuantity());
			purchasedBook.setPurchase(purchaseDb);
			purchasedBookService.save(purchasedBook);
			Book book = bookService.findBookById(shoppingCard.getBookId());
			int restNumberOfBook = book.getQuantity()- shoppingCard.getQuantity();
			book.setQuantity(restNumberOfBook);
			bookService.updateBook(book);
			shoppingCardService.deleteByUserIdAndBookId(shoppingCard.getUserId(), shoppingCard.getBookId());		
		}
		
		String successMessage = "Thanks for purchase! ";
		LoyaltyCard loyaltyCard = loyaltyCardService.findByUserId(user_id);
		Integer earnedPointsFromPurchase = (int) (totalPriceOfPurchase/1000);
		if(loyaltyCard != null) {
			if(loyaltyCard.getStatusCard().equals(LoyaltyCard.APPROVED)) {
				successMessage += "With this purchase you have won " + earnedPointsFromPurchase + " points on your Loyalty card.";
			}
		}

		if(points != null && points>0) {
			
			loyaltyCard.setPoints(loyaltyCard.getPoints() - points +earnedPointsFromPurchase);
			
			loyaltyCardService.update(loyaltyCard);
			
			successMessage += " By using your Loyalty card, you get a discount of " + discount + " dinars.";
					
		}

		model.addAttribute("successMessage", successMessage);
			
		return shoppingCartController.showShoppingCart(model, session);
	}
}

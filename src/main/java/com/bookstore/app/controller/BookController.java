package com.bookstore.app.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bookstore.app.model.Book;
import com.bookstore.app.model.Genre;
import com.bookstore.app.model.ShoppingCart;
import com.bookstore.app.model.User;
import com.bookstore.app.service.BookService;
import com.bookstore.app.service.GenreService;

@Controller
public class BookController {

	@Autowired
	private BookService bookService;

	@Autowired
	private GenreService genreService;

	@GetMapping("/allBooks")
	public String showAllBooks(Model model) {
		if (!model.containsAttribute("listOfBooks")) {
			List<Book> listOfBooks = bookService.getAllBooks();
			model.addAttribute("listOfBooks", listOfBooks);
		}
		return "books";
	}

	@GetMapping("/book/{id}")
	public String showBookDetails(Model model, @PathVariable Long id, HttpSession session) {
		Book book = bookService.findBookById(id);

		ShoppingCart shoppingCart = new ShoppingCart();
		shoppingCart.setBookId(book.getId());
		User currentUser = (User) session.getAttribute("currentUser");
		if (currentUser != null && currentUser.getRole().equals(currentUser.BUYER)) {
			shoppingCart.setUserId(currentUser.getId());
		}

		model.addAttribute("book", book);
		try {
			model.addAttribute("picture", Base64.getEncoder().encodeToString(book.getPicture()));
		} catch (Exception e) {

		}

		model.addAttribute("shoppingCart", shoppingCart);
		return "bookDetails";
	}

	@GetMapping("/book/create")
	public String prepareBookCreate(Model model) {
		Book book = new Book();
		List<Genre> allGenres = genreService.getAllGenres();
		model.addAttribute("book", book);
		model.addAttribute("allGenres", allGenres);
		return "bookCreate";
	}

	@PostMapping("/book/created")
	public String createBook(Model model, Book book, @RequestParam(name = "genreId", required = false) Long[] genreIds,
			@RequestParam MultipartFile file, HttpServletRequest request) {
		for (Long id : genreIds) {
			book.getGenres().add(genreService.findGenreById(id));
		}
		double averageMarkBook = (Math.random() * 10 + 1);
		averageMarkBook = Math.round(averageMarkBook * 100.0) / 100.0;
		book.setAverageMarkBook(averageMarkBook);
		try {
			if (file != null)
				book.setPicture(file.getBytes());
		} catch (IOException e) {

		}
		bookService.createBook(book);
		model.addAttribute("successMessage", "The book successfully created!");
		return showAllBooks(model);
	}

	@GetMapping("/book/update/{id}")
	public String prepareBookUpdate(Model model, @PathVariable Long id) {
		Book book = bookService.findBookById(id);
		List<Genre> allGenres = genreService.getAllGenres();
		model.addAttribute("book", book);
		model.addAttribute("allGenres", allGenres);
		try {
			model.addAttribute("picture", Base64.getEncoder().encodeToString(book.getPicture()));
		} catch (Exception e) {

		}
		return "bookUpdate";
	}

	@PostMapping("/book/update")
	public String updateBook(Model model, Book book, @RequestParam(name = "genreId", required = false) Long[] genreIds,
			@RequestParam(name = "file", required = false) MultipartFile file, HttpServletRequest request,
			HttpSession session) {
		for (Long id : genreIds) {
			book.getGenres().add(genreService.findGenreById(id));
		}
		try {
			if (file != null)
				book.setPicture(file.getBytes());
		} catch (IOException e) {

		}
		bookService.updateBook(book);
		model.addAttribute("successMessage", "The book successfully updated!");
		return showBookDetails(model, book.getId(), session);
	}

	@PostMapping("/allBooks")
	public String findByMinPrice(Model model, @RequestParam Double minPrice) {
		List<Book> allBooks = bookService.getAllBooks();
		List<Book> listOfBooks = new ArrayList<Book>();
		for (Book book : allBooks) {
			if (book.getPrice() > minPrice) {
				listOfBooks.add(book);
			}
		}
		model.addAttribute("listOfBooks", listOfBooks);
		return showAllBooks(model);
	}

	@PostMapping("/allBooksMax")
	public String findByMaxPrice(Model model, @RequestParam Double maxPrice, RedirectAttributes redirectAttributes) {
		List<Book> allBooks = bookService.getAllBooks();
		List<Book> listOfBooks = new ArrayList<Book>();
		for (Book book : allBooks) {
			if (book.getPrice() < maxPrice) {
				listOfBooks.add(book);
			}
		}
		redirectAttributes.addFlashAttribute("listOfBooks", listOfBooks);
		return "redirect:/allBooks";
	}
}

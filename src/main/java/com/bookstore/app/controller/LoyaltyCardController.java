package com.bookstore.app.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.bookstore.app.model.LoyaltyCard;
import com.bookstore.app.model.User;
import com.bookstore.app.service.LoyaltyCardService;
import com.bookstore.app.service.UserService;

@Controller
public class LoyaltyCardController {

	@Autowired
	private LoyaltyCardService loyaltyCardService;

	@Autowired
	private UserService userService;

	@GetMapping("/admin/loyalty")
	public String showAllCards(Model model) {
		List<LoyaltyCard> listOfCards = loyaltyCardService.findAll();
		Map<User, LoyaltyCard> mapOfCards = new HashMap<>();

		for (LoyaltyCard loyaltyCard : listOfCards) {
			User user = userService.findUserById(loyaltyCard.getUserId());
			mapOfCards.put(user, loyaltyCard);
		}
		model.addAttribute("mapOfCards", mapOfCards);
		return "loyaltyCards";
	}

	@GetMapping("/admin/loyaltyStatus")
	public void updateLoyaltyCard(@RequestParam(name = "loyaltyId") Long id, @RequestParam(name = "statusCard") String statusCard) {
		LoyaltyCard loyaltyCard = loyaltyCardService.findById(id);
		int statusCardParse = Integer.parseInt(statusCard);
		switch (statusCardParse) {
		case 1:
			loyaltyCard.setStatusCard(LoyaltyCard.APPROVED);
			loyaltyCard.setPoints(4);
			break;
		case 2:
			loyaltyCard.setStatusCard(LoyaltyCard.REJECTED);
		default:
			break;
		};
		loyaltyCardService.update(loyaltyCard);
		
	}

}

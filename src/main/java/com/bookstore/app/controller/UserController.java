package com.bookstore.app.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.bookstore.app.model.LoyaltyCard;
import com.bookstore.app.model.User;
import com.bookstore.app.service.LoyaltyCardService;
import com.bookstore.app.service.UserService;

@Controller
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private LoyaltyCardService loyaltyCardService;

	@Autowired
	private ServletContext servletContext;
	private String baseURL;

	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/allBooks";
	}

	@GetMapping("/")
	public String homePage(HttpServletRequest request) {
		return "redirect:" + request.getScheme() + ":/login";
	}

	@GetMapping("/admin/allUsers")
	public String showAllUsers(Model model) {
		List<User> allUsers = userService.getAllUsers();
		model.addAttribute("allUsers", allUsers);
		return "users";
	}

	@GetMapping("/registration")
	public String prepareRegistrationForm(Model model) {
		User user = new User();
		model.addAttribute("user", user);
		return "registration";
	}

	@PostMapping("/registration")
	public ModelAndView registerUser(User user, HttpServletRequest request, boolean loyaltyCard) {
		User userExist = userService.findUserByUsername(user.getUsername());

		if (userExist != null) {
			String message = "Username already exist!";

			ModelAndView registrationPage = new ModelAndView("registration");
			registrationPage.addObject("message", message);

			return registrationPage;
		}

		userService.createUser(user);

		user = userService.findUserByUsername(user.getUsername());

		sendRequestForLoyayltyCard(loyaltyCard, user);

		return new ModelAndView("login");
	}

	@GetMapping("/login")
	public String prepareLogin(Model model) {
		User user = new User();
		model.addAttribute("user", user);
		return "login";
	}

	@PostMapping(value = "/login")
	public ModelAndView checkLogin(@RequestParam String username, @RequestParam String password, HttpSession session,
			HttpServletResponse response) throws IOException {

		String message = "";
		try {
			User currentUser = userService.findUserByUsernameAndPassword(username, password);
			
			if (currentUser == null) {
				message = "Incorrect username or password";
				throw new Exception(message);
			}
			
			session.setAttribute("currentUser", currentUser);
			
			switch (currentUser.getRole()) {
			case User.BLOCKED:
				message = "You are blocked. Please contact administrator.";
				throw new Exception(message);
			case User.ADMIN:
				response.sendRedirect(servletContext.getContextPath() + "/admin/allUsers");
				break;
			case User.BUYER:
				response.sendRedirect(servletContext.getContextPath() + "/users/profile");
			default:
				response.sendRedirect(baseURL);
			}
			return null;
			
		} catch (Exception ex) {

			if (message.isEmpty()) {
				message = "Bad credentials!";
			}

			ModelAndView loginPage = new ModelAndView("login");
			loginPage.addObject("message", message);

			return loginPage;
		}
	}

	@GetMapping("/logout")
	public void logout(HttpSession session, HttpServletResponse response) throws IOException {
		session.invalidate();

		response.sendRedirect(baseURL);

	}

	private void sendRequestForLoyayltyCard(boolean loyaltyCard, User user) {
		if (!loyaltyCard) {
			return;
		}

		LoyaltyCard card = loyaltyCardService.findByUserId(user.getId());
		if (card != null) {
			card.setStatusCard(LoyaltyCard.HOLD);
			loyaltyCardService.update(card);
			user.setLoyaltyCardOnHold(true);
			return;
		}
		card = new LoyaltyCard();
		card.setUserId(user.getId());
		card.setPoints(0);
		card.setStatusCard(LoyaltyCard.HOLD);
		loyaltyCardService.save(card);
		user.setLoyaltyCardOnHold(true);
	}

	@GetMapping("/admin/allUsers/profile/{userId}")
	public String showProfileOfUsersByAdmin(Model model, @PathVariable Long userId) {
		User user = userService.findUserById(userId);
		model.addAttribute("user", user);
		return "users_admin";
	}

	@PostMapping("/admin/allUsers/profile/update")
	public String updateProfileOfUsersByAdmin(Model model, User user) {
		User userDb = userService.findUserById(user.getId());
		if(userDb.getRole().equals(User.ADMIN)) {
			if(!user.getRole().equals(User.ADMIN)) {
				String errorMessage = "You can't change ADMIN role";
				model.addAttribute("errorMessage", errorMessage);
				return showProfileOfUsersByAdmin(model, userDb.getId());
			}
		}
		
		userService.updateUser(user);
		model.addAttribute("successMessage", "The user successfully updated");
		return showAllUsers(model);
		
	}
	
	@GetMapping("/users/profile")
	public String showUserProfile(Model model, HttpSession session) {
		User currentUser = (User) session.getAttribute("currentUser");
		if(currentUser==null) {
			return "redirect:/allBooks";
		}
		currentUser = userService.findUserById(currentUser.getId());
		model.addAttribute("user", currentUser);
		return "user_profile";
	}

	@GetMapping("/users/profile/update")
	public String showUserProfileForUpdate(Model model, HttpSession session) {
		User currentUser = (User) session.getAttribute("currentUser");
		if(currentUser==null) {
			return "redirect:/allBooks";
		}
		currentUser = userService.findUserById(currentUser.getId());
		model.addAttribute("user", currentUser);
		return "user_profile_update";
	}

	@PostMapping("/users/profile/update")
	public String updateProfileOfUsers(Model model, User user, boolean loyaltyCard, HttpSession session) {
		userService.updateUser(user);
		sendRequestForLoyayltyCard(loyaltyCard, user);
		model.addAttribute("successMessage", "The user successfully updated");
		return showUserProfile(model, session);
	}
	
}

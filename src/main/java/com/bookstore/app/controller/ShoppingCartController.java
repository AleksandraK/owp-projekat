package com.bookstore.app.controller;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.bookstore.app.model.Book;
import com.bookstore.app.model.LoyaltyCard;
import com.bookstore.app.model.ShoppingCart;
import com.bookstore.app.model.User;
import com.bookstore.app.service.BookService;
import com.bookstore.app.service.LoyaltyCardService;
import com.bookstore.app.service.ShoppingCartService;

@Controller
public class ShoppingCartController {

	@Autowired
	private ShoppingCartService shoppingCartService;

	@Autowired
	private BookService bookService;

	@Autowired
	private BookController bookController;

	@Autowired
	private LoyaltyCardService loyaltyCardService;

	@GetMapping("/shoppingCart")
	public String showShoppingCart(Model model, HttpSession session) {
		User currentUser = (User) session.getAttribute("currentUser");
		if (currentUser == null) {
			return "redirect:/allBooks";
		}

		List<ShoppingCart> shoppingCarts = shoppingCartService.findByUserId(currentUser.getId());
		Map<ShoppingCart, Book> map = new HashMap<ShoppingCart, Book>();
		Double totalPrice = 0.0;
		for (ShoppingCart shoppingCart : shoppingCarts) {
			Book book = bookService.findBookById(shoppingCart.getBookId());
			map.put(shoppingCart, book);
			totalPrice += shoppingCart.getPrice();
		}
		model.addAttribute("shoppingCarts", map);

		LoyaltyCard loyaltyCard = loyaltyCardService.findByUserId(currentUser.getId());
		model.addAttribute("loyalty", loyaltyCard);

		Map<String, Integer> mapDiscount = new LinkedHashMap<String, Integer>();

		if (loyaltyCard != null) {
			Integer disountLimit = 10;
			if (loyaltyCard.getPoints() < disountLimit) {
				disountLimit = loyaltyCard.getPoints();
			}
			for (Integer i = 1; i <= disountLimit; i++) {

				String discount = "" + (i * 5) + "% (" + i + " poena)";
				if (i == 1) {
					discount = "" + (i * 5) + "% (" + i + " poen)";
				}
				mapDiscount.put(discount, i);
			}
		}
		
		model.addAttribute("totalPrice", totalPrice);
		model.addAttribute("mapDiscount", mapDiscount);

		return "shoppingCart";
	}

	@PostMapping("/addToShoppingCart")
	public String addToShoppingCart(Model model, ShoppingCart shoppingCart, HttpSession session) {

		if (shoppingCartService.findById(shoppingCart) != null) {
			ShoppingCart shoppingCartFromDb = shoppingCartService.findById(shoppingCart);
			Book book = bookService.findBookById(shoppingCart.getBookId());

			int bookQuantity = shoppingCart.getQuantity() + shoppingCartFromDb.getQuantity();
			if (bookQuantity > book.getQuantity()) {
				String errorMessage = "You've already had " + shoppingCartFromDb.getQuantity() + " "
						+ book.getNameOfBook() + " in your shopping cart. Maximum quantity is "
						+ (book.getQuantity() - shoppingCartFromDb.getQuantity() + ".");
				model.addAttribute("errorMessage", errorMessage);

				return bookController.showBookDetails(model, shoppingCart.getBookId(), session);

			} else {
				if (shoppingCart.getBookId().equals(shoppingCartFromDb.getBookId())) {
					shoppingCart.setQuantity(shoppingCartFromDb.getQuantity() + shoppingCart.getQuantity());
					shoppingCart.setPrice(shoppingCartFromDb.getPrice() + shoppingCart.getPrice());
					shoppingCartService.updateShoppingCart(shoppingCart);
				}
			}
		} else {
			shoppingCartService.createShoppingCart(shoppingCart);
		}

		return showShoppingCart(model, session);

	}

	@GetMapping("/shoppingCart/delete")
	public void deleteShoppingCart(@RequestParam(value = "userId") Long userId,
			@RequestParam(value = "bookId") Long bookId) {
		shoppingCartService.deleteByUserIdAndBookId(userId, bookId);
	}

	@PostMapping("/buyBook")
	public String buyBooks(Long userId) {

		List<ShoppingCart> shoppingCarts = shoppingCartService.findByUserId(userId);

		for (ShoppingCart shoppingCart : shoppingCarts) {
			Book book = bookService.findBookById(shoppingCart.getBookId());
			book.setQuantity(book.getQuantity() - shoppingCart.getQuantity());
			bookService.updateBook(book);
			shoppingCartService.deleteByUserIdAndBookId(shoppingCart.getUserId(), shoppingCart.getBookId());
		}

		return "redirect:/shoppingCart";

	}

}
package com.bookstore.app.controller;

import java.sql.Date;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.bookstore.app.model.Book;
import com.bookstore.app.model.Comment;
import com.bookstore.app.model.Purchase;
import com.bookstore.app.model.PurchasedBook;
import com.bookstore.app.model.User;
import com.bookstore.app.service.BookService;
import com.bookstore.app.service.CommentService;
import com.bookstore.app.service.PurchaseService;
import com.bookstore.app.service.PurchasedBookService;
import com.bookstore.app.service.UserService;

@Controller
public class CommentController {

	@Autowired
	CommentService commentService;

	@Autowired
	UserService userService;

	@Autowired
	BookService bookService;
	
	@Autowired
	PurchaseService purchaseService;
	
	@Autowired
	PurchasedBookService purchasedBookService;

	@GetMapping("/admin/comments")
	public String showCommentOnHold(Model model) {
		List<Comment> listOfComments = commentService.findAllByStatusComment(Comment.HOLD);
		Map<Comment, User> mapOfComments = new LinkedHashMap<>();
		Collections.sort(listOfComments);

		for (Comment comment : listOfComments) {
			User user = userService.findUserById(comment.getUserId());
			mapOfComments.put(comment, user);
		}
		model.addAttribute("mapOfComments", mapOfComments);
	
		return "comments";
	}

	@GetMapping("/admin/commentStatus")
	public void updateComment(@RequestParam(name = "commentId") Long id,
			@RequestParam(name = "statusComment") Integer statusComment) {
		Comment comment = commentService.findById(id);
//		int status = Integer.parseInt(statusKomentara);
		switch (statusComment) {
		case 1:
			comment.setStatusComment(Comment.APPROVED);
			break;
		case 2:
			comment.setStatusComment(Comment.REJECTED);
		default:
			break;
		}
		commentService.update(comment);
		Book book = bookService.findBookById(comment.getBookId());
		book.setAverageMarkBook((book.getAverageMarkBook()+comment.getMark())/2);
		bookService.updateBook(book);
	}
	
	@GetMapping("/book/comments/{id}")
	public String showCommentsOfBook(Model model, @PathVariable Long id, HttpSession session) {
		User user = (User) session.getAttribute("currentUser");
		if(user == null) {
			user = new User();
		} else {
			user = userService.findUserById(user.getId());
		}
		Book book = bookService.findBookById(id);
		List<Purchase> listOfPurchase = purchaseService.findAllByUserId(user.getId());
		for (Purchase purchase : listOfPurchase) {
			if(user.isPurchasedBook()) {
				break;
			}
			List<PurchasedBook> listOfPurchasedBooks = purchasedBookService.findAllByPurchaseId(purchase.getPurchaseId());
			for (PurchasedBook purchasedBook : listOfPurchasedBooks) {
				if(purchasedBook.getBook().getId().equals(id)) {
					user.setPurchasedBook(true);
					break;
				}
			}
		}
		
		model.addAttribute("user", user);
		model.addAttribute("book", book);
		Map<Comment, User> mapOfComments = new TreeMap<Comment, User>();
		List<Comment> listOfComment = commentService.findAllByStatusComment(Comment.APPROVED);
		for (Comment comment : listOfComment) {
			if (comment.getBookId().equals(id)) {
				User author = userService.findUserById(comment.getUserId());
				mapOfComments.put(comment, author);
			}
		}
		model.addAttribute("mapOfComments", mapOfComments);
		Comment comment = new Comment();
		model.addAttribute("comment", comment);
		return "comments_book";
	}

	@PostMapping("/comments/add")
	public String addComment(Model model, Comment comment, HttpSession session) {
		comment.setStatusComment(Comment.HOLD);
		Date dateOfComment = new Date(System.currentTimeMillis());
		comment.setDateOfComment(dateOfComment);
		
		commentService.save(comment);

		String successMessage = "Your comment will be send to administrator.";

		model.addAttribute("successMessage", successMessage);

		return showCommentsOfBook(model, comment.getBookId(), session);
	}

}

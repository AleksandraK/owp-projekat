package com.bookstore.app.dao;

import java.util.List;

import com.bookstore.app.model.User;


public interface UserDAO {

	public List<User> getAllUsers();
	
	public User findUserById(Long userId);
	
	public User findUserByUsername(String username);
	
	public User findUserByUsernameAndPassword(String username, String password);
	
	public void createUser(User user);
	
	public void updateUser(User user);
}

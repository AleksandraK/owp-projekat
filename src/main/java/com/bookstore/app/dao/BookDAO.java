package com.bookstore.app.dao;

import java.util.List;

import com.bookstore.app.model.Book;

public interface BookDAO {

	public List<Book> getBooks();
	
	public Book findBookById(Long id);
	
	public void updateBook(Book book);
	
	public void createBook(Book book);
}

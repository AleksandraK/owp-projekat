package com.bookstore.app.dao;

import java.util.List;
import java.util.Optional;

import com.bookstore.app.model.User;
import com.bookstore.app.model.Wishlist;

public interface WishlistDAO {

	public Optional<Wishlist> findById(Wishlist wishlist);
	
	public List<Wishlist> findAll();
	
	public List<Wishlist> findAllByUserId(Long userId);
	
	public List<Wishlist> findAllByBookId(Long bookId);
	
	public void save(Wishlist wishlist);
	
	public void delete(Wishlist wishlist);
	
	public void deleteByUserIdAndBookId(Wishlist wishlist);
	
	public List<Wishlist> findByUser(User user);
	
	public Wishlist findByBookAndUserId(Long book_id, Long user_id);
}

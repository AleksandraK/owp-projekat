package com.bookstore.app.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.bookstore.app.dao.PurchasedBookDAO;
import com.bookstore.app.model.Book;
import com.bookstore.app.model.Purchase;
import com.bookstore.app.model.PurchasedBook;
import com.bookstore.app.model.User;
import com.bookstore.app.service.BookService;
import com.bookstore.app.service.UserService;

@Repository
public class PurchasedBookDAOImpl implements PurchasedBookDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private BookService bookService;

	@Autowired
	private UserService userService;

	@Autowired
	private PurchaseDAOImpl purchaseService;

	private class PurchasedBookRowMapper implements RowMapper<PurchasedBook> {

		private Map<Long, PurchasedBook> purchasedBookMap = new LinkedHashMap<>();

		@Override
		public PurchasedBook mapRow(ResultSet rs, int rowNum) throws SQLException {

			Long purchasedBookId = rs.getLong("purchased_book_id");
			Long bookId = rs.getLong("book_id");
			Long purchaseId = rs.getLong("purchase_id");
			Integer totalQuantity = rs.getInt("total_quantity");
			Double totalPrice = rs.getDouble("total_price");
			Book book = bookService.findBookById(bookId);
			Long userId = rs.getLong("user_id");
			User user = userService.findUserById(userId);
			Purchase purchase = purchaseService.findById(purchaseId).get();
			Double price = rs.getDouble("price");
			Integer quantity = rs.getInt("quantity");
			PurchasedBook purchasedBook = new PurchasedBook();
			purchasedBook.setPurchasedBookId(purchasedBookId);
			purchasedBook.setBook(book);
			purchasedBook.setUser(user);
			purchasedBook.setPrice(price);
			purchasedBook.setQuantity(quantity);
			purchasedBook.setPurchase(purchase);
			purchasedBook.setTotalQuantity(totalQuantity);
			purchasedBook.setTotalPrice(totalPrice);
			purchasedBookMap.put(purchasedBookId, purchasedBook);
			return purchasedBook;
		}

		public List<PurchasedBook> getPurchasedBooks() {
			return new ArrayList<>(purchasedBookMap.values());
		}

	}

	@Override
	public Optional<PurchasedBook> findById(String id) {
		String sql = "SELECT pb.*, b.name_of_book, b.author,  SUM(pb.quantity) AS total_quantity,  SUM(pb.price) AS total_price FROM purchase pur LEFT JOIN purchased_book pb ON pur_purchase_id = pb.purchase_id LEFT JOIN book b ON pb.book_id = b.id where pur.purchase_id=?";

		PurchasedBookRowMapper rowCallbackHandler = new PurchasedBookRowMapper();
		jdbcTemplate.query(sql, rowCallbackHandler, id);

		PurchasedBook purchasedBook = rowCallbackHandler.getPurchasedBooks().get(0);

		return Optional.ofNullable(purchasedBook);
	}

	@Override
	public List<PurchasedBook> findAllByPurchaseId(Long purchaseId) {
		String sql = "SELECT pb.*, b.name_of_book, b.author,  SUM(pb.quantity) AS total_quantity, SUM(pb.price) AS total_price FROM purchase pur LEFT JOIN purchased_book pb ON pur.purchase_id = pb.purchase_id LEFT JOIN book b ON pb.book_id = b.id where pur.purchase_id=?";

		PurchasedBookRowMapper rowCallbackHandler = new PurchasedBookRowMapper();
		jdbcTemplate.query(sql, rowCallbackHandler, purchaseId);

		return rowCallbackHandler.getPurchasedBooks();
	}

	@Override
	public List<PurchasedBook> findAll() {
		String sql = "SELECT pb.*, b.name_of_book, b.author,  SUM(pb.quantity) AS total_quantity,  SUM(pb.price) AS total_price "
				+ " FROM purchase_book pb LEFT JOIN purchase pur ON pb.purchase_id = pur.purchase_id LEFT JOIN book b ON pb.book_id = b.id "
				+ " GROUP BY b.book_id";

		PurchasedBookRowMapper rowCallbackHandler = new PurchasedBookRowMapper();
		jdbcTemplate.query(sql, rowCallbackHandler);
		return rowCallbackHandler.getPurchasedBooks();
	}

	@Override
	public List<PurchasedBook> findAllByTimeOfPurchase(LocalDateTime startDate, LocalDateTime endDate) {
		String sql = "SELECT pb.*, b.name_of_book, b.author,  SUM(pb.quantity) AS total_quantity, SUM(pb.price) AS total_price FROM purchase pur LEFT JOIN purchased_book pb ON pur.purchase_id = pb.purchase_id LEFT JOIN book b ON pb.book_id = b.id WHERE time_of_purchase BETWEEN ? AND ? group by pb.book_id";

		PurchasedBookRowMapper rowCallbackHandler = new PurchasedBookRowMapper();
		jdbcTemplate.query(sql, rowCallbackHandler, startDate, endDate);
		return rowCallbackHandler.getPurchasedBooks();
	}

	@Override
	public void save(PurchasedBook purchasedBook) {
		String sql = "INSERT INTO purchased_book(book_id, user_id, price, quantity, purchase_id) " + "VALUES('"
				+ purchasedBook.getBook().getId() + "','" + purchasedBook.getUser().getId() + "','"
				+ purchasedBook.getPrice() + "','" + purchasedBook.getQuantity() + "','"
				+ purchasedBook.getPurchase().getPurchaseId() + "')";
		jdbcTemplate.update(sql);
	}

	@Override
	public List<PurchasedBook> findAllByPurchaseTimeOfPurchase(LocalDateTime dateTime) {
		dateTime = dateTime.minusHours(2);
		String dateTimeString = dateTime.toString();
		dateTimeString = dateTimeString.replace("T", " ");
		String sql = "SELECT pb.*, b.name_of_book, b.author,  SUM(pb.quantity) AS total_quantity,  SUM(pb.price) AS total_price  FROM purchase pur JOIN purchased_book pb ON pur.purchase_id = pb.purchase_id LEFT JOIN book b ON pb.book_id = b.id where pur.time_of_purchase like '"
				+ dateTimeString + "%' group by pb.book_id";

		PurchasedBookRowMapper rowCallbackHandler = new PurchasedBookRowMapper();
		jdbcTemplate.query(sql, rowCallbackHandler);
		return rowCallbackHandler.getPurchasedBooks();
	}

}

package com.bookstore.app.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.bookstore.app.dao.PurchaseDAO;
import com.bookstore.app.model.Purchase;
import com.bookstore.app.model.User;
import com.bookstore.app.service.UserService;

@Repository
public class PurchaseDAOImpl implements PurchaseDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private UserService userService;

	private class PurchaseRowMapper implements RowMapper<Purchase> {

		private Map<Long, Purchase> purchaseMap = new LinkedHashMap<>();

		@Override
		public Purchase mapRow(ResultSet rs, int rowNum) throws SQLException {

			Long purchaseId = rs.getLong("purchase_id");

			Long userId = rs.getLong("user_id");
			User user = userService.findUserById(userId);
			Double totalPriceOfPurchase = rs.getDouble("total_price_of_purchase");
			Integer numberOfPurchasedBooks = rs.getInt("number_of_purchased_books");
			Timestamp timeOfPurchaseSql = rs.getTimestamp("time_of_purchase");
			LocalDateTime timeOfPurchase = timeOfPurchaseSql.toLocalDateTime();
			Purchase purchase = new Purchase();
			purchase.setPurchaseId(purchaseId);
			purchase.setNumberOfPurchasedBooks(numberOfPurchasedBooks);
			purchase.setTotalPriceOfPurchase(totalPriceOfPurchase);
			purchase.setTimeOfPurchase(timeOfPurchase);
			purchase.setUser(user);
			purchaseMap.put(purchaseId, purchase);
			return purchase;
		}

		public List<Purchase> getPurchase() {
			return new ArrayList<>(purchaseMap.values());

		}

	}

	@Override
	public Optional<Purchase> findById(Long id) {
		String sql = "SELECT * FROM purchase WHERE purchase_id=?";

		PurchaseRowMapper rowCallbackHandler = new PurchaseRowMapper();
		jdbcTemplate.query(sql, rowCallbackHandler, id);

		Purchase purchase = rowCallbackHandler.getPurchase().get(0);

		return Optional.ofNullable(purchase);
	}

	@Override
	public List<Purchase> findAllByUserId(Long userId) {
		try {

			String sql = "SELECT * FROM purchase WHERE user_id=?";

			PurchaseRowMapper rowCallbackHandler = new PurchaseRowMapper();
			jdbcTemplate.query(sql, rowCallbackHandler, userId);

			return rowCallbackHandler.getPurchase();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Purchase> findAll() {
		String sql = "SELECT * FROM purchase";

		PurchaseRowMapper rowCallbackHandler = new PurchaseRowMapper();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getPurchase();
	}

	@Override
	public void save(Purchase purchase) {
		String sql = "INSERT INTO purchase(total_price_of_purchase, user_id, time_of_purchase, number_of_purchased_books) "
				+ "VALUES('" + purchase.getTotalPriceOfPurchase() + "','" + purchase.getUser().getId() + "','"
				+ Timestamp.valueOf(purchase.getTimeOfPurchase()) + "','" + purchase.getNumberOfPurchasedBooks() + "')";
		jdbcTemplate.update(sql);

	}

}

package com.bookstore.app.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bookstore.app.dao.WishlistDAO;
import com.bookstore.app.model.Book;
import com.bookstore.app.model.User;
import com.bookstore.app.model.Wishlist;
import com.bookstore.app.service.BookService;
import com.bookstore.app.service.UserService;

@Repository
public class WishlistDAOImpl implements WishlistDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private BookService bookService;

	@Autowired
	private UserService userService;

	private class WishlistRowMapper implements RowMapper<Wishlist> {

		@Override
		public Wishlist mapRow(ResultSet rs, int rowNum) throws SQLException {

			Long bookId = rs.getLong("book_id");
			Book book = bookService.findBookById(bookId);
			Long userId = rs.getLong("user_id");
			User user = userService.findUserById(userId);
			Wishlist wishlist = new Wishlist(book, user);
			return wishlist;
		}

	}

	@Override
	public Optional<Wishlist> findById(Wishlist wishlist) {
		try {
			String sql = "SELECT * FROM wishlist WHERE book_id=? AND user_id=?";
			Wishlist wishlistDb = jdbcTemplate
					.query(sql, new WishlistRowMapper(), wishlist.getBook().getId(), wishlist.getUser().getId()).get(0);
			return Optional.ofNullable(wishlistDb);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Wishlist> findAll() {
		String sql = "SELECT * FROM wishlist";
		return jdbcTemplate.query(sql, new WishlistRowMapper());
	}

	public List<Wishlist> findAllByUserId(Long userId) {
		String sql = "SELECT * FROM wishlist WHERE user_id=?";
		return jdbcTemplate.query(sql, new WishlistRowMapper(), userId);
	}

	public List<Wishlist> findAllByBookId(Long bookId) {
		String sql = "SELECT * FROM wishlist WHERE book_id=?";
		return jdbcTemplate.query(sql, new WishlistRowMapper(), bookId);
	}

	@Override
	public void save(Wishlist wishlist) {
		String sql = "INSERT INTO wishlist(book_id, user_id) " + "VALUES('"
				+ wishlist.getBook().getId() + "','" + wishlist.getUser().getId() + "')";
		jdbcTemplate.update(sql);
	}

	@Transactional
	@Override
	public void delete(Wishlist wishlist) {
		String sql = "DELETE from wishlist WHERE user_id=?";
		jdbcTemplate.update(sql, wishlist.getUser().getId());
	}

	@Transactional
	@Override
	public void deleteByUserIdAndBookId(Wishlist wishlist) {
		String sql = "DELETE from wishlist WHERE user_id=? AND book_id=?";
		jdbcTemplate.update(sql, wishlist.getUser().getId(), wishlist.getBook().getId());
	}

	@Override
	public List<Wishlist> findByUser(User user) {
		String sql = "SELECT * FROM wishlist WHERE user_id=?";
		return jdbcTemplate.query(sql, new WishlistRowMapper(), user.getId());
	}

	public Wishlist findByBookAndUserId(Long book_id, Long user_id) {
		String sql = "SELECT * FROM wishlist WHERE book_id=? AND user_id=?";
		return jdbcTemplate.query(sql, new WishlistRowMapper(), book_id, user_id).get(0);
	}

}

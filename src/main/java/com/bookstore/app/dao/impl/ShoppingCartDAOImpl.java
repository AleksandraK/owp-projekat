package com.bookstore.app.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.bookstore.app.dao.ShoppingCartDAO;
import com.bookstore.app.model.ShoppingCart;

@Repository
public class ShoppingCartDAOImpl implements ShoppingCartDAO{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	private class ShoppingCartRowMapper implements RowMapper<ShoppingCart> {

		@Override
		public ShoppingCart mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			Integer quantity = rs.getInt(index++);
			Double price = rs.getDouble(index++);
			Long bookId = rs.getLong(index++);
			Long userId = rs.getLong(index++);
			
			ShoppingCart shoppingCart = new ShoppingCart();
			shoppingCart.setBookId(bookId);
			shoppingCart.setQuantity(quantity);
			shoppingCart.setPrice(price);
			shoppingCart.setUserId(userId);
			return shoppingCart;
		}

	}

	@Override
	public List<ShoppingCart> getAll() {
			String sql = "SELECT * FROM shopping_cart";
			return jdbcTemplate.query(sql, new ShoppingCartRowMapper());
	}

	@Override
	public ShoppingCart findById(ShoppingCart shoppingCart) {
		try {
			String sql = "SELECT * FROM shopping_cart WHERE book_id=? AND user_id=?";
			ShoppingCart shoppingCartFromDb = jdbcTemplate
					.query(sql, new ShoppingCartRowMapper(), shoppingCart.getBookId(), shoppingCart.getUserId()).get(0);
			return shoppingCartFromDb;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void updateShoppingCart(ShoppingCart shoppingCart) {
		String sql = "UPDATE shopping_cart SET price = ?, quantity =?  WHERE book_id=? AND user_id=?";
		jdbcTemplate.update(sql, shoppingCart.getPrice(), shoppingCart.getQuantity(), shoppingCart.getBookId(), shoppingCart.getUserId());
	}

	@Override
	public void createShoppingCart(ShoppingCart shoppingCart) {
		String sql = "INSERT INTO shopping_cart(book_id, user_id, price, quantity) " + "VALUES(?,?,?,?)";
		jdbcTemplate.update(sql, shoppingCart.getBookId(), shoppingCart.getUserId(), shoppingCart.getPrice(), shoppingCart.getQuantity());
		
	}

	@Override
	public List<ShoppingCart> findByUserId(Long userId) {
		String sql = "SELECT * FROM shopping_cart WHERE user_id=?";
		return jdbcTemplate.query(sql, new ShoppingCartRowMapper(), userId);
	}

	@Override
	public void deleteByUserIdAndBookId(Long userId, Long bookId) {
		String sql = "DELETE from shopping_cart WHERE user_id=? AND book_id=?";
		jdbcTemplate.update(sql, userId, bookId);
		
	}

	@Override
	public ShoppingCart findByUserIdAndBookId(Long userId, Long bookId) {
		String sql = "SELECT * FROM  shopping_cart WHERE book_id=? AND user_id=?";
		return jdbcTemplate.query(sql, new ShoppingCartRowMapper(), userId, bookId).get(0);
	}

}

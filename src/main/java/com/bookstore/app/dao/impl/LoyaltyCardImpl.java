package com.bookstore.app.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.bookstore.app.dao.LoyaltyCardDAO;
import com.bookstore.app.model.LoyaltyCard;

@Repository
public class LoyaltyCardImpl implements LoyaltyCardDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private class LoyaltyCardRowMapper implements RowMapper<LoyaltyCard> {

		@Override
		public LoyaltyCard mapRow(ResultSet rs, int rowNum) throws SQLException {

			Long id = rs.getLong("id");
			Long userId = rs.getLong("user_id");
			Integer points = rs.getInt("points");
			String statusCard = rs.getString("status_card");
			LoyaltyCard loyaltyCard = new LoyaltyCard();
			loyaltyCard.setId(id);
			loyaltyCard.setUserId(userId);
			loyaltyCard.setPoints(points);
			loyaltyCard.setStatusCard(statusCard);
			return loyaltyCard;
		}

	}

	@Override
	public LoyaltyCard findById(Long id) {
		try {
			String sql = "SELECT * FROM loyalty_card WHERE id=?";
			LoyaltyCard loyaltyCard = jdbcTemplate.query(sql, new LoyaltyCardRowMapper(), id).get(0);
			return loyaltyCard;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<LoyaltyCard> findAll() {
		String sql = "SELECT * FROM loyalty_card";
		return jdbcTemplate.query(sql, new LoyaltyCardRowMapper());
	}

	@Override
	public LoyaltyCard findByUserId(Long userId) {
		try {
			String sql = "SELECT * FROM loyalty_card WHERE user_id=?";
			return jdbcTemplate.query(sql, new LoyaltyCardRowMapper(), userId).get(0);
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public void save(LoyaltyCard loyaltyCard) {
		String sql = "INSERT INTO loyalty_card(user_id, points, status_card) " + "VALUES('"
				+ loyaltyCard.getUserId() + "','" + loyaltyCard.getPoints() + "','"
				+ loyaltyCard.getStatusCard() + "')";

		jdbcTemplate.update(sql);
	}

	@Override
	public void update(LoyaltyCard loyaltyCard) {
		String sql = "UPDATE loyalty_card SET points =?, status_card=? WHERE id=?";
		jdbcTemplate.update(sql, loyaltyCard.getPoints(),
				loyaltyCard.getStatusCard(), loyaltyCard.getId());
	}

	@Override
	public void delete(LoyaltyCard loyaltyCard) {
		String sql = "DELETE FROM loyalty_card WHERE id=?";
		jdbcTemplate.update(sql, loyaltyCard.getId());
	}

	@Override
	public List<LoyaltyCard> findAllByStatusCard(String statusCard) {
		String sql = "SELECT * FROM loyalty_card WHERE status_card=?";
		return jdbcTemplate.query(sql, new LoyaltyCardRowMapper(), statusCard);
	}

	@Override
	public void updatePoints(LoyaltyCard loyaltyCard) {
		String sql = "UPDATE loyalty_card SET points =?, WHERE id=?";
		jdbcTemplate.update(sql, loyaltyCard.getPoints(), loyaltyCard.getId());
	}

	@Override
	public double usePointsForDiscount(int points, double price) {
		if (points > 0 && points <= 10) {
			double discount = points * 5;
			price = price - (price * 100 / discount);
		}
		return price;
	}

}

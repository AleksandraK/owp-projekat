package com.bookstore.app.dao.impl;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.bookstore.app.dao.CommentDAO;
import com.bookstore.app.model.Comment;

@Repository
public class CommentDAOImpl implements CommentDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private class CommentRowMapper implements RowMapper<Comment> {

		@Override
		public Comment mapRow(ResultSet rs, int rowNum) throws SQLException {

			Long id = rs.getLong("id");
			Long userId = rs.getLong("user_id");
			Long bookId = rs.getLong("book_id");
			String textComment = rs.getString("text_comment");
			Integer mark = rs.getInt("mark");
			String statusComment = rs.getString("status_comment");
			Date dateOfComment = rs.getDate("date_of_comment");
			Comment comment = new Comment();
			comment.setId(id);
			comment.setUserId(userId);
			comment.setBookId(bookId);
			comment.setStatusComment(statusComment);
			comment.setTextComment(textComment);
			comment.setMark(mark);
			comment.setDateOfComment(dateOfComment);
			return comment;
		}

	}

	@Override
	public Comment findById(Long id) {
		try {
			String sql = "SELECT * FROM comment WHERE id=?";
			return jdbcTemplate.query(sql, new CommentRowMapper(), id).get(0);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Comment> findAll() {
		String sql = "SELECT * FROM comment";
		return jdbcTemplate.query(sql, new CommentRowMapper());
	}

	@Override
	public Comment findByUserId(Long userId) {
		try {
			String sql = "SELECT * FROM comment WHERE user_id=?";
			return jdbcTemplate.query(sql, new CommentRowMapper(), userId).get(0);
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public void save(Comment comment) {
		String sql = "INSERT INTO comment(book_id, user_id, text_comment, date_of_comment, mark, status_comment) " + "VALUES('"
				+ comment.getBookId() + "','" + comment.getUserId() + "','" + comment.getTextComment() + "','" +
				comment.getDateOfComment() + "','" +comment.getMark() + "','" + comment.getStatusComment() + "')";

		jdbcTemplate.update(sql);
	}

	@Override
	public void update(Comment comment) {
		String sql = "UPDATE comment SET mark =?, status_comment=? WHERE id=?";
		jdbcTemplate.update(sql, comment.getMark(),
				comment.getStatusComment(), comment.getId());
	}

	@Override
	public void delete(Comment comment) {
		String sql = "DELETE FROM comment WHERE id=?";
		jdbcTemplate.update(sql, comment.getId());
	}

	@Override
	public List<Comment> findAllByStatusComment(String statusComment) {
		String sql = "SELECT * FROM comment WHERE status_comment=?";
		return jdbcTemplate.query(sql, new CommentRowMapper(), statusComment);
	}

}

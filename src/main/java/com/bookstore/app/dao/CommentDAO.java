package com.bookstore.app.dao;

import java.util.List;

import com.bookstore.app.model.Comment;

public interface CommentDAO {

	public Comment findById(Long id);
	
	public List<Comment> findAll();
	
	public Comment findByUserId(Long userId);
	
	public void save(Comment comment);
	
	public void update(Comment comment);
	
	public void delete(Comment comment);
	
	public List<Comment> findAllByStatusComment(String statusComment);
	
}

package com.bookstore.app.dao;

import java.util.List;

import com.bookstore.app.model.LoyaltyCard;

public interface LoyaltyCardDAO {
	
	public LoyaltyCard findById(Long id);
	
	public List<LoyaltyCard> findAll();
	
	public LoyaltyCard findByUserId(Long userId);
	
	public void save(LoyaltyCard loyaltyCard);
	
	public void update(LoyaltyCard loyaltyCard);
	
	public void delete(LoyaltyCard loyaltyCard);
	
	public List<LoyaltyCard> findAllByStatusCard(String statusCard);
	
	public void updatePoints(LoyaltyCard loyaltyCard);
	
	public double usePointsForDiscount(int points, double price);
	
	
	
}
